# TroveKube

A simple reimplementation of the [Baserock Trove] based on containers
to enable Docker and Kubernetes deployments.


# Using Docker Compose

We assume that you have a basic understanding of `docker` and that
you already have installed `docker` and `docker-compose` in your system.


    cd docker-compose/
    sh run.sh

> Note: You might have to run the command avobe with `sudo` if your
> user is not in the `docker` group.

Now you should be able to see:

- Lorry-controller admin interface: <http://localhost:12765/1.0/status-html>
- CGit frotend: <http://localhost:8080/>
- Non-admin Lorry-controller status page: <http://localhost:8080/lc-status.html>

[Baserock Trove]: http://wiki.baserock.org/Trove/
